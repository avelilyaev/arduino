//This code from master branch
#include "libs.h"

//Указатель на массив слов
word *ArrayOfWords;
//-------------------------------------------------------------------------------------------

//Указатель на битовый буфер
char *Buffer;
//-------------------------------------------------------------------------------------------

//Указатель на матрицу для транспонирования
char **Matrix;
//-------------------------------------------------------------------------------------------

//Функция инициализации массива слов
int InitializeArrayOfWords()
{
   Serial.println("Enter number of words: ");
   int countOfWords = readWord();
   Serial.print("Count of words = "); Serial.println(countOfWords);
   
   ArrayOfWords = (word*) malloc(countOfWords*sizeof(word));
    for (int i = 0; i < countOfWords; i++)
    {
      Serial.print("Word[");Serial.print(i);Serial.print("] = ");
      ArrayOfWords[i] = readWord();
      Serial.println(ArrayOfWords[i]);
    }
    
   return countOfWords;
}
//-------------------------------------------------------------------------------------------

void MainActions(word *array, int len)
{ 
  //Выделяем массив из кучи  
  Buffer = (char*) malloc(len * 16 * sizeof(char));

  //Записываем массив слов в буфер как набор битов
  long Length = RecordToBuffer(array, Buffer, len);

  Serial.println("Enter length of sequences");
  Serial.print("length = ");
  int N = readWord();
  //Расчитываем количество операций трансформирования
  int countOfOperations = Length / (N * N);

  //Принимаем решение о корректности введенного N
  if (countOfOperations < 1)
  {
    Serial.print("message > Incorrect N for current length of array");
  }
  else
  {
    //Индексы для формированиея матриц из линии и наоборот
    int ptr1 = 0; int ptr2 = 0;

    //Выделяем память под матрицу для трансформирования
    Matrix = (char **) malloc(N * sizeof(char *));
    for (int i = 0; i < N; i++)
      Matrix[i] = (char *) malloc(N * sizeof(char));
    
    //Выполняем действия расчитанное количество раз
    for (int i = 0; i < countOfOperations; i++)
    {
      //Формируем квадрат
      for (int j = 0; j < N; j++)
      {
        for (int k = 0; k < N; k++)
        {
          Matrix[j][k] = Buffer[ptr1];
          ptr1++;
        }
      }
      //Выводим текущий исходный квадрат
      Serial.println("original matrix:");
      outputMatrix(Matrix, N);

      //Трансформируем квадрат
      transformMatrix(Matrix, N);

      //Выводим трансформированный квадрат
      Serial.println("transformed matrix:");
      outputMatrix(Matrix, N);

      //Отделяем линией для читабельности
      Serial.println("-----------------------------------------");

      //Разворачиваем квадрат обратно в линию
      for (int i = 0; i < N; i++)
      {
        for (int j = 0; j < N; j++)
        {
          Buffer[ptr2] = Matrix[i][j];
          ptr2++;
        }
      } 
    }//Конец обработки 

    //Выводим получившийся битовый поток
    Serial.println("The final bitsream:");
    outputArray(Buffer, ptr2);

    //Освобождение памяти, выделенной под динамические массивыы
    for (int i = 0; i < N; i++)
      free (Matrix[i]); 
    free (Matrix);
  } 
   free (Buffer);
   free(ArrayOfWords);
}
//--------------------------------------------------------------------------------------------

void setup() {
  //Вызываем функцию, инициализирующую параметры последовательного порта
  initialize();
}

void loop() {
  //Инициализируем массив слов
  int len = InitializeArrayOfWords();

  //Вызываем функцию, выполняющую все действия
  MainActions(ArrayOfWords, len);

  //Останавливаем бесконечный цикл "loop"
  pause();
}

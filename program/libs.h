#include "Arduino.h"
//Этот файл написан для упрощения читабельности кода в основном файле этого скетча
//--------------------------------------------------------------------------------
//Функция для остановки бесконечного цикла "loop"
void pause()
{
  Serial.println();
  Serial.print("message > executed");
  while(1){}
}
//------------------------------------------------------------------------------------------

//Меняет местами две переменные
template<typename T>
void swap(T &first, T &second)
{
  char temporary = first;
  first = second;
  second = temporary;
}
//-------------------------------------------------------------------------------------------

//Функция трансформирования матрицы
void transformMatrix(char **matrix, int N)
{
  for (int i = 0; i < N; i++)
  {
    for (int j = i; j < N; j++)
    {
      if (i != j)
      {
        swap(matrix[i][j], matrix[j][i]);
      }
    }
  }
}
//-------------------------------------------------------------------------------------------

//Функция вывода матрицы
void outputMatrix(char **matrix, int N)
{
  for (int i = 0; i < N; i++)
    {
      for (int j = 0; j < N; j++)
      {
        Serial.print(matrix[i][j]);
        Serial.print(" ");
      }
      Serial.println();
    }
}
//-------------------------------------------------------------------------------------------

//Функция вывода одномерного массива
template<typename T>
void outputArray(T (&array), int N)
{
  for (int i = 0; i < N; i++)
    {
        Serial.print(array[i]);
        if (i == N - 1)
        {
          Serial.print(".");
        }
        else
        {
          Serial.print(", ");
        }
    }
}
//-------------------------------------------------------------------------------------------

//Функция для ввода слова с клавиатуры
word readWord()
{
  return (word)Serial.parseInt();
}
//-------------------------------------------------------------------------------------------

String ConvertToBinaryChar(word value)
{
    //Записываем строковое представление двочиного value в переменную binValue
    char buf[16];
    itoa(value, buf, 2);
    String binValue = buf;

    //Считаем длину получившейся строки
    byte countOfSymbols = 0;
    for (int i = 0; i < 16; i++)
    {
      if (binValue[i] == '0' || binValue[i] == '1')
      {
        countOfSymbols++;
      }      
    }

    //Добиваем слева старшими нулями, чтобы фиксировать длину (16 бит)
    for (int i = 0; i < 16 - countOfSymbols; i++)
    {
     binValue = '0' + binValue;
    }

    //Возвращаем строку из 16 двочиных значений
    return binValue;
}

int RecordToBuffer(word * array, char * Buffer, int len)
{
  long ptr = 0;
  String binaryValue;

  //Проходимся по массиву слов 
  for (int i = 0; i < len; i++)
  {  
    //Получаем бинарное текстовое представление текущего слова
    binaryValue = ConvertToBinaryChar(array[i]);

    //Дописываем текущее двоичное слово в буфер
    for (int j = 0; j < 16; j++)
    {
        Buffer[ptr] = binaryValue[j];
        ptr++;  
    } 
  }
  //Занесли весь массив ArrayOfWords в Буфер в виде набора битов--------------------
  return ptr;
}

//Инициализирующая функция
void initialize()
{
  Serial.begin(9600);
  Serial.setTimeout(60000);
}
